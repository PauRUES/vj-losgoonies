#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Player.h"
#include "Game.h"
#include <mmsystem.h>


#define JUMP_ANGLE_STEP 4
#define JUMP_HEIGHT 66
#define FALL_STEP 4
#define INJURED_TIME 200

#define SCREEN_X 32
#define SCREEN_Y 28


enum PlayerAnims
{
	STAND_LEFT, STAND_RIGHT, MOVE_LEFT, MOVE_RIGHT, MOVE_DOWN, JUMP_LEFT, JUMP_RIGHT, ATTACK_LEFT, ATTACK_RIGHT, STAND_LEFT_INJURED, STAND_RIGHT_INJURED, MOVE_LEFT_INJURED, MOVE_RIGHT_INJURED, JUMP_LEFT_INJURED, JUMP_RIGHT_INJURED
};


void Player::init(const glm::ivec2 &tileMapPos, ShaderProgram &shaderProgram)
{
	godmode = false;
	lp = 81;
	exp = 0;
	injured = false;
	time_injured = 0; 
	kids = 0;
	texProgram = shaderProgram; 
	bJumping = false;
	climbing = false; 
	changing_screen = false; 
	spritesheet.loadFromFile("images/player_aux.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(32,32), glm::vec2(1/3.f, 1/11.f), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(15);
	sprite->setAnimationSpeed(STAND_LEFT, 8);
	sprite->addKeyframe(STAND_LEFT, glm::vec2(0.f, 4 / 11.f)); 

	sprite->setAnimationSpeed(STAND_RIGHT, 8);
	sprite->addKeyframe(STAND_RIGHT, glm::vec2(0.0f, 1 / 11.f));

	sprite->setAnimationSpeed(MOVE_LEFT, 8);
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 4 / 11.f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(1 / 3.f, 4 / 11.f));
	sprite->addKeyframe(MOVE_LEFT, glm::vec2(2 / 3.f, 4 / 11.f));

	sprite->setAnimationSpeed(MOVE_RIGHT, 8);
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 1 / 11.f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(1 / 3.f, 1 / 11.f));
	sprite->addKeyframe(MOVE_RIGHT, glm::vec2(2 / 3.f, 1 / 11.f));

	sprite->setAnimationSpeed(MOVE_DOWN, 8);
	sprite->addKeyframe(MOVE_DOWN, glm::vec2(0.f, 0.f));
	sprite->addKeyframe(MOVE_DOWN, glm::vec2(1 / 3.f, 0.f));

	sprite->setAnimationSpeed(ATTACK_LEFT, 8);
	sprite->addKeyframe(ATTACK_LEFT, glm::vec2(0.f, 6 / 11.f));

	sprite->setAnimationSpeed(ATTACK_RIGHT, 8);
	sprite->addKeyframe(ATTACK_RIGHT, glm::vec2(0.f, 3 / 11.f));

	sprite->setAnimationSpeed(JUMP_LEFT, 8);
	sprite->addKeyframe(JUMP_LEFT, glm::vec2(0.f, 5 / 11.f));

	sprite->setAnimationSpeed(JUMP_RIGHT, 8);
	sprite->addKeyframe(JUMP_RIGHT, glm::vec2(0.f, 2 / 11.f));


	sprite->setAnimationSpeed(STAND_LEFT_INJURED, 8);
	sprite->addKeyframe(STAND_LEFT_INJURED, glm::vec2(0.f, 8 / 11.f));
	sprite->addKeyframe(STAND_LEFT_INJURED, glm::vec2(0.f, 4 / 11.f));
	sprite->addKeyframe(STAND_LEFT_INJURED, glm::vec2(0.f, 8 / 11.f));

	sprite->setAnimationSpeed(STAND_RIGHT_INJURED, 8);
	sprite->addKeyframe(STAND_RIGHT_INJURED, glm::vec2(0.0f, 7 / 11.f));
	sprite->addKeyframe(STAND_RIGHT_INJURED, glm::vec2(0.0f, 1 / 11.f));
	sprite->addKeyframe(STAND_RIGHT_INJURED, glm::vec2(0.0f, 7 / 11.f));

	sprite->setAnimationSpeed(MOVE_LEFT_INJURED, 8);
	sprite->addKeyframe(MOVE_LEFT_INJURED, glm::vec2(0.f, 8/ 11.f));
	sprite->addKeyframe(MOVE_LEFT_INJURED, glm::vec2(1 / 3.f, 4 / 11.f));
	sprite->addKeyframe(MOVE_LEFT_INJURED, glm::vec2(2 / 3.f, 8 / 11.f));

	sprite->setAnimationSpeed(MOVE_RIGHT_INJURED, 8);
	sprite->addKeyframe(MOVE_RIGHT_INJURED, glm::vec2(0.f, 7/ 11.f));
	sprite->addKeyframe(MOVE_RIGHT_INJURED, glm::vec2(1 / 3.f, 1 / 11.f));
	sprite->addKeyframe(MOVE_RIGHT_INJURED, glm::vec2(2 / 3.f, 7 / 11.f));

	sprite->setAnimationSpeed(JUMP_LEFT_INJURED, 8);
	sprite->addKeyframe(JUMP_LEFT_INJURED, glm::vec2(0.f, 10 / 11.f));

	sprite->setAnimationSpeed(JUMP_RIGHT_INJURED, 8);
	sprite->addKeyframe(JUMP_RIGHT_INJURED, glm::vec2(0.f, 9 / 7.f));
		
	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
	
}

void Player::update(int deltaTime)
{
	sprite->update(deltaTime);
	int endScreen = map->endScreen(posPlayer);

	if (Game::instance().getKey(103))
		godmode = !godmode;
	
	/*if (map->collisionEnemy(posPlayer)) {
		injured = true; 
	}*/

	if (injured && time_injured <= INJURED_TIME) time_injured += 4;
	else injured = false; 
	
	if (endScreen != -1 && !changing_screen) {
		
		glm::ivec2 newPosPlayer = map->changeScreen(endScreen, glm::vec2(SCREEN_X, SCREEN_Y),posPlayer, texProgram);
		posPlayer.x = newPosPlayer.x;
		if (bJumping) startY = newPosPlayer.y;
		posPlayer.y = newPosPlayer.y; 
		updateInformation();
	}
	
	if (Game::instance().getSpecialKey(GLUT_KEY_LEFT))
	{
		if (!climbing) {

			if (sprite->animation() != MOVE_LEFT && !injured)
				sprite->changeAnimation(MOVE_LEFT);
			else if (sprite->animation() != MOVE_LEFT_INJURED && injured)
				sprite->changeAnimation(MOVE_LEFT_INJURED);

			posPlayer.x -= 2;
			int enemy_attack = map->collisionEnemy(posPlayer, glm::ivec2(16, 16));
			
			if (map->collisionJar(posPlayer, glm::ivec2(16, 16))) {
				exp = map->takeJar();
				gain_exp(8);
			}
			
			if (map->collisionKey(posPlayer, glm::ivec2(16, 16))) {
				 key = map->takeKey();
				 updateInformation();
			}

			if (map->collisionLock(posPlayer, glm::ivec2(16, 16))) {
				map->openDoor(key);
				updateInformation();
			}

			if (map->collisionKid(posPlayer, glm::ivec2(16, 16))) {
				kids++; 
				updateInformation();
			}

			if (enemy_attack != -1 && !injured && !godmode) {
				injured = true;
				time_injured = 0; 
				lose_lp(2);
				sprite->changeAnimation(MOVE_LEFT_INJURED); 
				PlaySound(TEXT("sounds/damaged.wav"), NULL, SND_FILENAME | SND_ASYNC);
			}
			if (map->collisionMoveLeft(posPlayer, glm::ivec2(16, 16)))
			{	
				posPlayer.x += 2;
				sprite->changeAnimation(STAND_LEFT);
			}
		}
	}
	else if (Game::instance().getSpecialKey(GLUT_KEY_RIGHT))
	{
		if (!climbing) {

			if (sprite->animation() != MOVE_RIGHT && !injured)
				sprite->changeAnimation(MOVE_RIGHT);
			else if (sprite->animation() != MOVE_RIGHT_INJURED && injured)
				sprite->changeAnimation(MOVE_RIGHT_INJURED);


			posPlayer.x += 2;
			if (map->collisionJar(posPlayer, glm::ivec2(16, 16))) {
				exp = map->takeJar();
				gain_exp(8);
			}

			if (map->collisionKey(posPlayer, glm::ivec2(16, 16))) {
				key = map->takeKey();
				updateInformation();
			}

			if (map->collisionLock(posPlayer, glm::ivec2(16, 16))) {
				map->openDoor(key);
				updateInformation();
			}

			if (map->collisionKid(posPlayer, glm::ivec2(16, 16))) {
				kids++;
				updateInformation();
			}
			int enemy_attack = map->collisionEnemy(posPlayer, glm::ivec2(16, 16));
			if (enemy_attack != -1 && !injured && !godmode) {
				time_injured = 0;
				sprite->changeAnimation(MOVE_RIGHT_INJURED);
				PlaySound(TEXT("sounds/damaged.wav"), NULL, SND_FILENAME | SND_ASYNC);
				lose_lp(2); 
				//lp -= enemy_attack;
				injured = true; 
			}
			if (map->collisionMoveRight(posPlayer, glm::ivec2(16, 16)))
			{
				posPlayer.x -= 2;
				sprite->changeAnimation(STAND_RIGHT);
			}
		}
	}
	else if (Game::instance().getKey(97)) {
		PlaySound(TEXT("sounds/hit.wav"), NULL, SND_FILENAME | SND_ASYNC);
		if (sprite->animation() == MOVE_LEFT || sprite->animation() == STAND_LEFT) {
			sprite->changeAnimation(ATTACK_LEFT);
			int enemy_attack = map->attackEnemyLeft(posPlayer, glm::ivec2(16, 16));
			if (enemy_attack != -1) {
				map->attack_enemy(enemy_attack); 
				gain_exp(2);
			}

		}
		else if (sprite->animation() == MOVE_RIGHT || sprite->animation() == STAND_RIGHT) {
			sprite->changeAnimation(ATTACK_RIGHT);
			int enemy_attack = map->attackEnemyRight (posPlayer, glm::ivec2(16, 16));
			if (enemy_attack != -1) {
				map->attack_enemy(enemy_attack);
				gain_exp(2);
			}
		}
	}
	else
	{
		int enemy_attack_right = map->collisionEnemy(posPlayer, glm::ivec2(16, 16));
		int enemy_attack_left = map->collisionEnemy(posPlayer, glm::ivec2(16, 16));
		
		if ((enemy_attack_right != -1 || enemy_attack_left !=-1) && !injured) {
			injured = true; 
			lose_lp(2); 
		}

		if (sprite->animation() == MOVE_LEFT && !injured)
			sprite->changeAnimation(STAND_LEFT);
		else if (sprite->animation() == MOVE_RIGHT && !injured )
			sprite->changeAnimation(STAND_RIGHT);
		else if (sprite->animation() == MOVE_LEFT_INJURED && injured)
			sprite->changeAnimation(STAND_LEFT_INJURED);
		else if (sprite->animation() == MOVE_RIGHT_INJURED && injured)
			sprite->changeAnimation(STAND_RIGHT_INJURED);
		else if (sprite->animation() == STAND_LEFT_INJURED && !injured)
			sprite->changeAnimation(STAND_LEFT);
		else if (sprite->animation() == STAND_RIGHT_INJURED && !injured)
			sprite->changeAnimation(STAND_RIGHT);
		else if (sprite->animation() == STAND_LEFT && injured)
			sprite->changeAnimation(STAND_LEFT_INJURED);
		else if (sprite->animation() == STAND_RIGHT && injured)
			sprite->changeAnimation(STAND_RIGHT_INJURED);
		else if (sprite->animation() == ATTACK_LEFT)
			sprite->changeAnimation(STAND_LEFT);
		else if (sprite->animation() == ATTACK_RIGHT)
			sprite->changeAnimation(STAND_RIGHT);


	}

	if (bJumping)
	{
		jumpAngle += JUMP_ANGLE_STEP;
		if (jumpAngle == 180)
		{
			bJumping = false;
			posPlayer.y = startY;
			if (sprite->animation() == JUMP_LEFT)
				sprite->changeAnimation(STAND_LEFT);
			else
				sprite->changeAnimation(STAND_RIGHT);
		}
		else
		{
			if (posYMax == 66)
				posPlayer.y = int(startY - posYMax * sin(3.14159f * jumpAngle / 180));
			else
				posPlayer.y = int(startY - (startY - posYMax) * sin(3.14159f * jumpAngle / 180));
			if (jumpAngle > 90) {
				bJumping = !map->collisionMoveDown(posPlayer, glm::ivec2(16, 16), &posPlayer.y);
				if (!bJumping) {
					if (sprite->animation() == JUMP_LEFT)
						sprite->changeAnimation(STAND_LEFT);
					else
						sprite->changeAnimation(STAND_RIGHT);
				}
			}
			else {
				if (map->collisionMoveUp(posPlayer, glm::ivec2(16, 16), &posPlayer.y) && jumpAngle < 92) {
					posYMax = posPlayer.y;
					jumpAngle = 92;
					posPlayer.y = int(startY - (startY - posYMax) * sin(3.14159f * jumpAngle / 180));
				}
			}
		}
	}
	else if(climbing){
		if (map->lianaup(posPlayer, glm::ivec2(16, 16), &posPlayer.y) && Game::instance().getSpecialKey(GLUT_KEY_UP)) {
			posPlayer.y -=1;
		}

		else if (!map->lianaup(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
			climbing = false; 
			sprite->changeAnimation(STAND_LEFT);
		}

		if (map->lianadown(posPlayer, glm::ivec2(16, 16), &posPlayer.y) && Game::instance().getSpecialKey(GLUT_KEY_DOWN)) {
			posPlayer.y += 1;
		}
		else if (!map->lianadown(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
			climbing = false;
			sprite->changeAnimation(STAND_LEFT);
		}
	}
	else
	{
		posPlayer.y += FALL_STEP;
		if (map->collisionMoveDown(posPlayer, glm::ivec2(16, 16), &posPlayer.y))
		{
			if (Game::instance().getSpecialKey(GLUT_KEY_UP))
			{
				if (map->lianaup(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
					climbing = true; 
					posPlayer.y -= 1;
					sprite->changeAnimation(MOVE_DOWN);
					
				}
				else if (map->puerta(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
					/*se cambia de mapa*/
				}
				else {
					PlaySound(TEXT("sounds/jump.wav"), NULL, SND_FILENAME | SND_ASYNC);
					bJumping = true;
					jumpAngle = 0;
					jumpAngleMax = 180.f;
					posYMax = 66;
					startY = posPlayer.y;
					if (sprite->animation() == MOVE_LEFT || sprite->animation() == STAND_LEFT)
						sprite->changeAnimation(JUMP_LEFT);
					else if (sprite->animation() == MOVE_RIGHT || sprite->animation() == STAND_RIGHT)
						sprite->changeAnimation(JUMP_RIGHT);

				}
			}
			else if (Game::instance().getSpecialKey(GLUT_KEY_DOWN))
			{
				if (map->lianadown(posPlayer, glm::ivec2(16, 16), &posPlayer.y)) {
					climbing = true;
					sprite->changeAnimation(MOVE_DOWN);
					posPlayer.y += 1;
				}
			}
		}
	}



	


	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
}

void Player::render()
{
	sprite->render();
}

void Player::setTileMap(TileMap *tileMap)
{
	map = tileMap;
}

void Player::setPosition(const glm::vec2 &pos)
{
	posPlayer = pos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posPlayer.x), float(tileMapDispl.y + posPlayer.y)));
}

void Player::lose_lp(int times) { //times indica el numero de veces que se resta 5 a la vida (algunas trampas hacen mas dao, otras menos...)
	if (!godmode) {
		lp -= times;
		bars->updateBar(lp, -times, 0, texProgram);
	}

}

void Player::gain_lp(int times) { //times indica las veces que gana 5 de vida (dependiendo de la exp al cambiar de pantalla por ejemplo)
	if (!godmode) {
		lp += times;
		if (lp > 81)
			lp = 81;
		bars->updateBar(lp, times, 0, texProgram);
	}
}

void Player::lose_exp(int times) { //dependiendo del enemigo, trampa...sss
	exp -= times;
	if (exp < 0)
		exp = 0;
	bars->updateBar(exp, -times, 1, texProgram);
}

void Player::gain_exp(int times) { //dependiendo del enemigo da mas o menos exp, coger las bolsas de dinero...
	exp += times;
	if (exp > 81)
		exp = 81;
	bars->updateBar(exp, times, 1, texProgram);
}


int Player::getLp() {
	return lp;
}

int Player::getExp() {
	return exp;
}

void Player::resetKeys() {
	key = 0;
	updateInformation();
}

bool Player::doorcollition() {
	return map->puerta(posPlayer, glm::ivec2(16, 16), &posPlayer.y);
}

void Player::setBars(TileMap* tileMap)
{
	bars = tileMap;
}

void Player::setInformation(TileMap* tileMap)
{
	information = tileMap;
}

void Player::updateInformation() {
	information->updateInformation(key, kids, map->getScreen(), texProgram);
}
