#include <cmath>
#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "Enemy.h"


enum EnemyAnims {
	MOVE_LEFT, MOVE_RIGHT
};

Enemy::Enemy()
{

}

void Enemy::init(int id_enemy, const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json file, int tSize)
{
	id = id_enemy; 
	dead = false; 
	texProgram = shaderProgram;
	typeEnemy = file["type"];
	tileSize = tSize; 
	startX = file["startX"] * tileSize;
	startY = file["startY"] * tileSize;
	speed = file["speed"]; 
	width_attack = file["width_attack"];
	attack = file["attack"];
	spritesheet.loadFromFile("images/enemy.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(32, 32), glm::vec2(1 / 2.f, 1 / 4.f), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(2);

	if (typeEnemy == "Calavera") {
		sprite->setAnimationSpeed(MOVE_LEFT, 8);
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 0.f));
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(1 / 2.f, 0.f));


		sprite->setAnimationSpeed(MOVE_RIGHT, 8);
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 1 / 4.f));
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(1 / 2.f, 1 / 4.f));
	}
	else {
		sprite->setAnimationSpeed(MOVE_LEFT, 8);
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(0.f, 3 / 4.f));
		sprite->addKeyframe(MOVE_LEFT, glm::vec2(1 / 2.f, 3 / 4.f));

		sprite->setAnimationSpeed(MOVE_RIGHT, 8);
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(0.f, 2 / 4.f));
		sprite->addKeyframe(MOVE_RIGHT, glm::vec2(1 / 2.f, 2 / 4.f));
	}

	posEnemy.x = startX ;
	posEnemy.y = startY; 
	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posEnemy.x), float(tileMapDispl.y + posEnemy.y)));



}

void Enemy::update(int deltaTime)
{

	sprite->update(deltaTime);

	if (sprite->animation() == MOVE_RIGHT) {
		posEnemy.x = posEnemy.x +int (speed *deltaTime);
		if (posEnemy.x > startX) {
			sprite->changeAnimation(MOVE_LEFT);
		}
	}
	else {
		posEnemy.x = posEnemy.x - int(speed* deltaTime);
		if (posEnemy.x < (startX - (width_attack *tileSize))) {
			sprite->changeAnimation(MOVE_RIGHT);
		}
	}
	/*if (posEnemy.x == 90) {
		sprite->changeAnimation(MOVE_RIGHT);
	}
	else if (attackAngle == 180) {
		sprite->changeAnimation(MOVE_LEFT);
		attackAngle = 0;
	}*/

	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posEnemy.x), float(tileMapDispl.y + posEnemy.y)));
}

void Enemy::render()
{
	if (!dead)
		sprite->render();
}

void Enemy::attacked()
{
	dead = true; 
}

glm::ivec2 Enemy::getPosition()
{
	return posEnemy;
}

bool Enemy::isDead()
{
	return dead;
}

void Enemy::setPosition(const glm::vec2& pos)
{
	posEnemy = pos;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posEnemy.x), float(tileMapDispl.y + posEnemy.y)));
}
