#ifndef _ENEMY_INCLUDE
#define _ENEMY_INCLUDE

#include "Sprite.h"
#include <json.hpp>


using namespace std;
using json = nlohmann::json;


class Enemy {

public:
	Enemy();
	void init(int id_enemy, const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json file, int tileSize);
	void update(int deltaTime);
	void render();
	void attacked(); 
	glm::ivec2 getPosition(); 
	bool isDead(); 
	void setPosition(const glm::vec2& pos); 

	int attack;
	string typeEnemy;


private:

	Texture spritesheet;
	ShaderProgram texProgram;
	Sprite* sprite;
	glm::ivec2 tileMapDispl, posEnemy;
	int id,width_attack, startX, startY,tileSize;
	float speed; 
	bool dead; 
};


#endif // _ENEMY_INCLUDE