#include <iostream>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include "Scene.h"
#include "Game.h"
#include <windows.h>
#include <GL/freeglut_std.h>



#define SCREEN_X 32
#define SCREEN_Y 28

#define INIT_PLAYER_X_TILES 2
#define INIT_PLAYER_Y_TILES 5


Scene::Scene()
{
	map = NULL;
	player = NULL;
}

Scene::~Scene()
{
	/*if (map != NULL)
		delete map; */
	for (auto kv : layout) {
		if (kv.second)
			delete kv.second;
	}
	if (player != NULL)
		delete player;
}


void Scene::init()
{
	initShaders();
	layout.clear();
	TileMap* map;
	//map = TileMap::createTileMap("levels/menu.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 2);
	map = TileMap::createTileMap("levels/menu.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;
	map = TileMap::createTileMap("levels/press_start.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 1);
	map->layer = 1;
	layout[map->layer] = map;
	projection = glm::ortho(0.f, float(SCREEN_WIDTH - 1), float(SCREEN_HEIGHT - 1), 0.f);
	currentTime = 0.0f;
	menu = true;
	instructions = false;
	bplayer = false;
	gameover = false;
	credits = false;
	scene = 0;
	//change_scene1();
}

void Scene::update(int deltaTime)
{
	currentTime += deltaTime;
	if (bplayer) {
		player->update(deltaTime);
		layout[2]->update_screen(deltaTime);
	}

	if (gameover || credits) {
		if (gameover) {
			Sleep(4000);
			gameover = false;
		}
		else if (credits) {
			Sleep(8000);
			credits = false;
		}
		init();
	}

	if (Game::instance().getKey(13) && menu) {
		menu = false;
		instructions = true;
		Sleep(1000);
		change_instructions();
	}
	else if (Game::instance().getKey(13) && instructions) {
		instructions = false;
		scene = 1;
		Sleep(1000);
		change_scene1();
	}
	else if (scene != 0 && Game::instance().getSpecialKey(GLUT_KEY_UP) && player->doorcollition()) {
		if (scene == 1 && player->numberKids() == 1) {
			scene += 1;
			Sleep(1000);
			layout[0]->changeScene(scene, texProgram);
			change_scene2();
		}
		if (scene == 2 && player->numberKids() == 2) {
			scene += 1;
			Sleep(1000);
			layout[0]->changeScene(scene, texProgram);
			change_scene3();
		}
		if (scene == 3 && player->numberKids() == 3) {
			scene += 1;
			Sleep(1000);
			layout[0]->changeScene(scene, texProgram);
			change_scene4();
		}
		else if (scene == 4 && player->numberKids() == 4) {
			scene += 1;
			Sleep(1000);
			layout[0]->changeScene(scene, texProgram);
			change_scene5();
		}
		else if (scene == 5 && player->numberKids() == 5) {
			Sleep(1000);
			change_credits();
		}
	}
	else if (scene != 0 && player->getLp() <= 0) {
		game_over();
	}

	if (Game::instance().getKey(49) || Game::instance().getKey(50) || Game::instance().getKey(51) || Game::instance().getKey(52) || Game::instance().getKey(53)) {
		if (Game::instance().getKey(49)) {
			Sleep(1000);
			change_scene1();
			scene = 1;
			layout[0]->changeScene(scene, texProgram);
		}
		else if (Game::instance().getKey(50)) {
			Sleep(1000);
			change_scene2();
			scene = 2;
			layout[0]->changeScene(scene, texProgram);
		}
		else if (Game::instance().getKey(51)) {
			Sleep(1000);
			change_scene3();
			scene = 3;
			layout[0]->changeScene(scene, texProgram);

		}
		else if (Game::instance().getKey(52)) {
			Sleep(1000);
			change_scene4();
			scene = 4;
			layout[0]->changeScene(scene, texProgram);

		}
		else if (Game::instance().getKey(53)) {
			Sleep(1000);
			change_scene5();
			scene = 5;
			layout[0]->changeScene(scene, texProgram);

		}
	}

	if (bplayer) {
		if (Game::instance().getKey(104)) {
			player->giveKids(1);
		}
		else if (Game::instance().getKey(106)) {
			player->giveKids(2);

		}
		else if (Game::instance().getKey(107)) {
			player->giveKids(3);

		}

		else if (Game::instance().getKey(108)) {
			player->giveKids(4);

		}
		else if (Game::instance().getKey(109)) {
			player->giveKids(5);
		}
		player->updateInformation();
	}
}

void Scene::render()
{
	glm::mat4 modelview;

	texProgram.use();
	texProgram.setUniformMatrix4f("projection", projection);
	texProgram.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
	modelview = glm::mat4(1.0f);

	texProgram.setUniformMatrix4f("modelview", modelview);
	texProgram.setUniform2f("texCoordDispl", 0.f, 0.f);
	
	for (int i = 0; i < layout.size(); ++i) {
		layout[i]->render();
	}

	if (bplayer)
		player->render();
}


void Scene::initShaders()
{
	Shader vShader, fShader;

	vShader.initFromFile(VERTEX_SHADER, "shaders/texture.vert");
	if(!vShader.isCompiled())
	{
		cout << "Vertex Shader Error" << endl;
		cout << "" << vShader.log() << endl << endl;
	}
	fShader.initFromFile(FRAGMENT_SHADER, "shaders/texture.frag");
	if(!fShader.isCompiled())
	{
		cout << "Fragment Shader Error" << endl;
		cout << "" << fShader.log() << endl << endl;
	}
	texProgram.init();
	texProgram.addShader(vShader);
	texProgram.addShader(fShader);
	texProgram.link();
	if(!texProgram.isLinked())
	{
		cout << "Shader Linking Error" << endl;
		cout << "" << texProgram.log() << endl << endl;
	}
	texProgram.bindFragmentOutput("outColor");
	vShader.free();
	fShader.free();
}

void Scene::change_scene1() {
	TileMap* map;
	player = new Player();
	player->init(glm::ivec2(SCREEN_X, SCREEN_Y), texProgram);
	map = TileMap::createTileMap("levels/punctuation.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;
	player->setInformation(map);
	map = TileMap::createTileMap("levels/bars.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 1);
	map->layer = 1;
	layout[map->layer] = map;
	player->setBars(map);
	map = TileMap::createTileMap("levels/escena1.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(3 * map->getTileSize(), 18 * map->getTileSize()));
	player->setTileMap(map);
	bplayer = true;
}

void Scene::change_scene2() {
	TileMap* map;
	player->resetKeys();
	map = TileMap::createTileMap("levels/escena2.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(3 * map->getTileSize(), 21 * map->getTileSize()));
	player->setTileMap(map);
	if (player->getExp() == 81)
		player->gain_lp(2);
}

void Scene::change_scene3() {
	TileMap* map;
	player->resetKeys();
	map = TileMap::createTileMap("levels/escena3.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(3 * map->getTileSize(), 22 * map->getTileSize()));
	player->setTileMap(map);
	if (player->getExp() == 81)
		player->gain_lp(2);
}

void Scene::change_scene4() {
	TileMap* map;
	player->resetKeys();
	map = TileMap::createTileMap("levels/escena4.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(3 * map->getTileSize(), 8	* map->getTileSize()));
	player->setTileMap(map);
	if (player->getExp() == 81)
		player->gain_lp(2);
}

void Scene::change_scene5() {
	TileMap* map;
	player->resetKeys();
	map = TileMap::createTileMap("levels/escena5.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 2);
	map->layer = 2;
	layout[map->layer] = map;
	player->setPosition(glm::vec2(3 * map->getTileSize(), 8 * map->getTileSize()));
	player->setTileMap(map);
	if (player->getExp() == 81)
		player->gain_lp(2);
}

void Scene::change_instructions() {
	TileMap* map;
	map = TileMap::createTileMap("levels/instructions.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;

}

void Scene::change_credits() {
	layout.clear();
	TileMap* map;
	map = TileMap::createTileMap("levels/credits.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;
	PlaySound(TEXT("sounds/credits.wav"), NULL, SND_FILENAME | SND_ASYNC);
	credits = true;
	bplayer = false;
}

void Scene::game_over() {
	Sleep(1000);
	layout.clear();
	TileMap* map;
	map = TileMap::createTileMap("levels/game_over.json", glm::vec2(SCREEN_X, SCREEN_Y), texProgram, 0, 0);
	map->layer = 0;
	layout[map->layer] = map;
	PlaySound(TEXT("sounds/game-over.wav"), NULL, SND_FILENAME | SND_ASYNC);
	bplayer = false;
	gameover = true;
}



