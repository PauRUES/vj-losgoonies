#include "Key.h"

Key::Key()
{

}

void Key::init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json jsonFile, int tileSize)
{
 
	taken = false; 

	id_key= jsonFile["id_key"];
	idDoorToOpen = jsonFile["door_to_open"];

	int startX = jsonFile["posX"];
	int startY= jsonFile["posY"];

	texProgram = shaderProgram;
	spritesheet.loadFromFile("images/key.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(16, 16), glm::vec2(1.f, 1.f), &spritesheet, &shaderProgram);

	tileMapDispl = tileMapPos;
	posKey.x = startX * tileSize; 
	posKey.y = startY * tileSize; 
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posKey.x), float(tileMapDispl.y + posKey.y)));

}



void Key::render()
{
	if (!taken) sprite->render(); 
}

int Key::takeKey()
{
	taken = true; 
	return idDoorToOpen;
}

glm::ivec2 Key::getPosition()
{
	return posKey;
}
