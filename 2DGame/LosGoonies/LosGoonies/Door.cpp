#include "Door.h"

Door::Door()
{
}

void Door::init(const glm::ivec2& tileMapPos, const json jsonFile, glm::vec2& minCoords, ShaderProgram& program)
{
	loadDoor(tileMapPos,jsonFile,program); 
	prepareArrays(minCoords, program); 
}

void Door::loadDoor(const glm::ivec2& tileMapPos, const json jsonFile, ShaderProgram& program)
{
	id = jsonFile["id_door"]; 
	open = false; 
	width = jsonFile["width"];
	height = jsonFile["height"]; 
	initialX = jsonFile["initialX"];
	initialY = jsonFile["initialY"];
	finalX = jsonFile["finalX"];
	finalY = jsonFile["finalY"]; 

	tilesheetSize.x = jsonFile["tilesheetSizeX"]; 
	tilesheetSize.y = jsonFile["tilesheetSizeY"];
	tileTexSize = glm::vec2(1.f / tilesheetSize.x, 1.f / tilesheetSize.y);

	tileSize = jsonFile["tileheight"];
	blockSize = jsonFile["tilewidth"];

	mapSize.x = jsonFile["mapSizeX"];
	mapSize.y = jsonFile["mapSizeY"]; 

	string tilesheetFile = jsonFile["source"];
	tilesheet.loadFromFile(tilesheetFile, TEXTURE_PIXEL_FORMAT_RGBA);
	tilesheet.setWrapS(GL_CLAMP_TO_EDGE);
	tilesheet.setWrapT(GL_CLAMP_TO_EDGE);
	tilesheet.setMinFilter(GL_NEAREST);
	tilesheet.setMagFilter(GL_NEAREST);

	map_door = new int[mapSize.x * mapSize.y];
	memset(&map_door[0], 0, sizeof(int) * mapSize.x * mapSize.y);
	
	int k = 0; 
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			map_door[(i+initialY) * mapSize.x + (initialX+j)] = jsonFile["data"][k++];
		}
	}

	tileMapDispl = tileMapPos;
	posLock.x = (initialX - 1) * tileSize; 
	posLock.y = (initialY + 2) * tileSize; 
	spritesheetLock.loadFromFile("images/lock.png", TEXTURE_PIXEL_FORMAT_RGBA);
	spriteLock = Sprite::createSprite(glm::ivec2(16, 16), glm::vec2(1.f, 1.f), &spritesheetLock, &program);
	spriteLock->setPosition(glm::vec2(float(tileMapDispl.x + posLock.x), float(tileMapDispl.y +posLock.y)));

	kidTaken = false; 
	posKid.x = (initialX+0.5 ) * tileSize;
	posKid.y = (initialY + 2) * tileSize;
	spritesheetKid.loadFromFile("images/kid.png", TEXTURE_PIXEL_FORMAT_RGBA);
	spriteKid = Sprite::createSprite(glm::ivec2(32, 32), glm::vec2(1.f, 1.f), &spritesheetKid, &program);
	spriteKid->setPosition(glm::vec2(float(tileMapDispl.x + posKid.x), float(tileMapDispl.y + posKid.y)));



}

void Door::render()
{
	if (open) {
		glEnable(GL_TEXTURE_2D);
		tilesheet.use();
		glBindVertexArray(vao);
		glEnableVertexAttribArray(posLocation);
		glEnableVertexAttribArray(texCoordLocation);
		glDrawArrays(GL_TRIANGLES, 0, 6 * 32 * 28);
		glDisable(GL_TEXTURE_2D);

		if ( !kidTaken) spriteKid->render(); 
	}
		
	spriteLock->render();

}

void Door::openDoor(int id_key)
{
	if(id_key == id ) open = true; 
}

bool Door::isOpen()
{
	return open;
}

bool Door::takeKid()
{
	if (!kidTaken) {
		kidTaken = true;
		return true;
	}
	else return false; 
}

glm::ivec2 Door::getPositionLock()
{
	return posLock;
}

glm::ivec2 Door::getPositionKid()
{
	return posKid;
}

void Door::prepareArrays(const glm::vec2& minCoords, ShaderProgram& program)
{
	int tile, nTiles = 0;
	glm::vec2 posTile, texCoordTile[2], halfTexel;
	vector<float> vertices;
	
	halfTexel = glm::vec2(0.5f / tilesheet.width(), 0.5f / tilesheet.height());
	for (int j = 0; j < mapSize.y; j++)
	{
		for (int i = 0; i < mapSize.x; i++)
		{
			tile = map_door[j * mapSize.x + i];
			if (tile != 0)
			{
				// Non-empty tile
				nTiles++;
				posTile = glm::vec2(minCoords.x + i * tileSize, minCoords.y + j * tileSize);
				texCoordTile[0] = glm::vec2(float((tile - 1) % tilesheetSize.x) / tilesheetSize.x, float((tile - 1) / tilesheetSize.x) / tilesheetSize.y);
				texCoordTile[1] = texCoordTile[0] + tileTexSize;
				//texCoordTile[0] += halfTexel;
				texCoordTile[1] -= halfTexel;
				// First triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				// Second triangle
				vertices.push_back(posTile.x); vertices.push_back(posTile.y);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[0].y);
				vertices.push_back(posTile.x + blockSize); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[1].x); vertices.push_back(texCoordTile[1].y);
				vertices.push_back(posTile.x); vertices.push_back(posTile.y + blockSize);
				vertices.push_back(texCoordTile[0].x); vertices.push_back(texCoordTile[1].y);
			}
		}
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 24 * nTiles * sizeof(float), &vertices[0], GL_STATIC_DRAW);
	posLocation = program.bindVertexAttribute("position", 2, 4 * sizeof(float), 0);
	texCoordLocation = program.bindVertexAttribute("texCoord", 2, 4 * sizeof(float), (void*)(2 * sizeof(float)));
}
