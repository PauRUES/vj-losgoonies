#ifndef _DOOR_INCLUDE
#define _DOOR_INCLUDE


#include <json.hpp>
#include "Sprite.h"

using json = nlohmann::json;


class Door {
public:

	Door();
	void init(const glm::ivec2& tileMapPos, const json jsonFile, glm::vec2& minCoords, ShaderProgram& program);
	void loadDoor(const glm::ivec2& tileMapPos, const json jsonFile, ShaderProgram& program);
	void render();

	void openDoor(int id_key); 
	bool isOpen(); 
	bool takeKid(); 

	glm::ivec2 getPositionLock();
	glm::ivec2 getPositionKid();

private:
	void prepareArrays(const glm::vec2& minCoords, ShaderProgram& program);

private:
	GLuint vao;
	GLuint vbo;
	GLint posLocation, texCoordLocation;
	glm::ivec2 tilesheetSize, mapSize,tileMapDispl,posLock,posKid;
	Texture tilesheet, spritesheetLock, spritesheetKid;
	glm::vec2 tileTexSize;
	Sprite* spriteLock, *spriteKid;

	int id, initialX, initialY, finalX, finalY;
	int width, height; 
	int tileSize, blockSize;
	bool open,kidTaken;

	int* map_door; 
};





#endif //_DOOR_INCLUDE