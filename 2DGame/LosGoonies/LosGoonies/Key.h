#ifndef _KEY_INCLUDE
#define _KEY_INCLUDE

#include <json.hpp>
#include "Sprite.h"

using json = nlohmann::json;

class Key {

public:
	Key();
	void init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json jsonFile, int tileSize);
	void render();
	int takeKey(); 
	glm::ivec2 getPosition();

private:
	Texture spritesheet;
	ShaderProgram texProgram;
	Sprite* sprite;
	glm::ivec2 tileMapDispl, posKey;

	int id_key, idDoorToOpen;
	bool taken; 


}; 

#endif //_KEY_INCLUDE
