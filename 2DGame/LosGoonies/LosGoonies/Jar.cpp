#include "Jar.h"

Jar::Jar()
{
}

void Jar::init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json jsonFile, int tileSize)
{
	taken = false;

	exp = jsonFile["exp"];
	int startX = jsonFile["posX"];
	int startY = jsonFile["posY"];

	texProgram = shaderProgram;
	spritesheet.loadFromFile("images/frasco.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(16, 16), glm::vec2(1.f, 1.f), &spritesheet, &shaderProgram);

	tileMapDispl = tileMapPos;
	posJar.x = startX * tileSize;
	posJar.y = startY * tileSize;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posJar.x), float(tileMapDispl.y + posJar.y)));

}

void Jar::render()
{
	if (!taken) sprite->render();
}

int Jar::takeJar()
{
	taken = true;
	return exp;
}

glm::ivec2 Jar::getPosition()
{
	return posJar;
}
