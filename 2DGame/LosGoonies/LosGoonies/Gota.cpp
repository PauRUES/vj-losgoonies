#include "Gota.h"

#include <json.hpp>

using json = nlohmann::json;

#define WAIT_TO_FALL 200
#define TRANSITION_TIME 100


enum GotaAnims
{
	SMALL_GOTA, MEDIUM_GOTA, BIG_GOTA, HEAD_GOTA, FLOOR_GOTA
};


Gota::Gota()
{
}

void Gota::init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json jsonFile, int tileSize)
{

	int iniPosX = jsonFile["posX"];
	int iniPosY = jsonFile["posY"]; 
	height = jsonFile["height"]; 
	timeToFall = 0; 
	timeTrasition = 0; 
	falling = false; 
	
	
	spritesheet.loadFromFile("images/gota.png", TEXTURE_PIXEL_FORMAT_RGBA);
	sprite = Sprite::createSprite(glm::ivec2(16, 16), glm::vec2(1 / 5.f, 1.f), &spritesheet, &shaderProgram);
	sprite->setNumberAnimations(5);

	sprite->setAnimationSpeed(SMALL_GOTA, 8);
	sprite->addKeyframe(SMALL_GOTA, glm::vec2(0.f, 1.f));


	sprite->setAnimationSpeed(MEDIUM_GOTA, 8);
	sprite->addKeyframe(MEDIUM_GOTA, glm::vec2(1/5.f, 1.f));

	sprite->setAnimationSpeed(BIG_GOTA, 8);
	sprite->addKeyframe(BIG_GOTA, glm::vec2(2/5.f, 1.f));

	sprite->setAnimationSpeed(HEAD_GOTA, 8);
	sprite->addKeyframe(HEAD_GOTA, glm::vec2(3/5.f,1.f));

	sprite->setAnimationSpeed(FLOOR_GOTA, 8);
	sprite->addKeyframe(FLOOR_GOTA, glm::vec2(4/5.f, 1.f));

	sprite->changeAnimation(0);
	tileMapDispl = tileMapPos;

	posGota.x = iniPosX * tileSize;
	posInicial = posGota.y = iniPosY * tileSize;
	posFinal = (iniPosY + height) * tileSize;
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posGota.x), float(tileMapDispl.y + posGota.y)));

}

void Gota::render()
{
	sprite->render();




	/*if (falling) {
		if (time < TRANSITION_TIME) time++;
		else {
			if (sprite->animation() == SMALL_GOTA) sprite->changeAnimation(MEDIUM_GOTA);
			else if((sprite->animation() == MEDIUM_GOTA)) sprite->changeAnimation(BIG_GOTA);
			els if 

		}
	}*/



}

void Gota::update(int deltaTime)
{
	sprite->update(deltaTime);

	if (transition && timeTrasition < (TRANSITION_TIME+50)) {
		timeTrasition += 4;
	}
	else if (transition && timeTrasition >= (TRANSITION_TIME+50)) {
		timeTrasition = 0;
		transition = false; 
	}
	
	
	if (!falling && !transition && timeToFall < WAIT_TO_FALL) {
		sprite->changeAnimation(SMALL_GOTA);
		posGota.y = posInicial;
		timeToFall += 4;

	}
	else if (!falling && !transition && timeToFall >= WAIT_TO_FALL) {
		if (timeTrasition < TRANSITION_TIME) {
			if (sprite->animation() == SMALL_GOTA) sprite->changeAnimation(MEDIUM_GOTA);
			timeTrasition += 4; 
		}
		else {
			falling = true;
			timeTrasition = 0;
		}
		
	}


	if (falling) {

		if (posGota.y == posFinal) {
			falling = false;
			timeToFall = 0;
			transition = true; 
			sprite->changeAnimation(FLOOR_GOTA);
		}
		else {
			posGota.y += 1;
			if ((sprite->animation() == MEDIUM_GOTA)) sprite->changeAnimation(BIG_GOTA);
		}

	}
	sprite->setPosition(glm::vec2(float(tileMapDispl.x + posGota.x), float(tileMapDispl.y + posGota.y)));
}

glm::ivec2 Gota::getPosition()
{
	return glm::ivec2();
}
