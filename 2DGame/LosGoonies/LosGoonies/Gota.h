#ifndef _GOTA_INCLUDE
#define _GOTA_INCLUDE

#include <json.hpp>
#include "Sprite.h"

using json = nlohmann::json;

class Gota {

public:
	Gota();
	void init(const glm::ivec2& tileMapPos, ShaderProgram& shaderProgram, const json jsonFile, int tileSize);
	void render();
	void update(int deltaTime); 
	glm::ivec2 getPosition();

private:
	Texture spritesheet;
	ShaderProgram texProgram;
	Sprite* sprite;
	glm::ivec2 tileMapDispl, posGota;

	int id_jar, exp, height, timeToFall, timeTrasition, fallingPosition, posInicial, posFinal;
	bool falling,transition;


};

#endif //_GOTA_INCLUDE